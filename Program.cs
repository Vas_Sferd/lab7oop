﻿using System;
using System.Collections.Generic;
using System.IO;

namespace lab7oop
{
    delegate void Transaction();
    delegate void InDepartamentStudentsLessThan7(int actual_count);
    delegate void InGroupStudentsMoreThan5(string group_name, int actual_count);

    static class EventManager
    {
        public static event InDepartamentStudentsLessThan7 InDepartamentStudentsLessThan7;
        public static event InGroupStudentsMoreThan5 InGroupStudentsMoreThan5;
        public static event Transaction Transaction;

        public static void Call_Transaction() => Transaction();
        public static void Call_InDepartamentStudentsLessThan7(int actual_count)
            => InDepartamentStudentsLessThan7(actual_count);
        public static void Call_InGroupStudentsMoreThan5(string group_name, int actual_count)
            => InGroupStudentsMoreThan5(group_name, actual_count);
    }

    class Department
    {
        public List<StudentsGroup> StudentsGroups { get; set; } = new List<StudentsGroup>();

        public Department()
        {
            EventManager.Transaction += new Transaction(CheckStudentsCount);
        }
        public void CheckStudentsCount()
        {
            int summ = 0;
            foreach (var group in StudentsGroups)
            {
                summ += group.Count();
            }
            if (summ < 7)
            {
                EventManager.Call_InDepartamentStudentsLessThan7(summ);
            }
        }
    }

    class StudentsGroup
    {
        public string GroupName { get; set; }
        List<Student> Students { get; set; } = new List<Student>();

        public StudentsGroup(string group_name)
        {
            GroupName = group_name;
        }

        public int Count() => Students.Count;
        public void Add(string name)
        {
            Students.Add(new Student(name));
            EventManager.Call_Transaction();
            CheckStudentsCount();
        }
        public void Del(string name)
        {
            Students.Remove(new Student(name));
            EventManager.Call_Transaction();
        }
        public void CheckStudentsCount()
        {
            if (Count() > 5)
            {
                EventManager.Call_InGroupStudentsMoreThan5(GroupName, Count());
            }
        }
    }

    class Student
    {
        public string Name { get; set; }
        public Student(string name)
        {
            Name = name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                return Name.Equals(((Student)obj).Name);
            }
            catch
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }
    }

    class Logger
    {
        private readonly StreamWriter log;

        public Logger(string path)
        {
            log = new StreamWriter(path, true);
            log.WriteLine("/*****************/");
            EventManager.InDepartamentStudentsLessThan7 += new InDepartamentStudentsLessThan7(Log_InDepartamentStudentsLessThan7);
            EventManager.InGroupStudentsMoreThan5 += new InGroupStudentsMoreThan5(Log_InGroupStudentsMoreThan5);
        }

        public void Log_InDepartamentStudentsLessThan7(int actual_count)
        {
            log.WriteLine("Количество студентов на кафедре меньше 7 и равно " + actual_count);
        }
        public void Log_InGroupStudentsMoreThan5(string group_name, int actual_count)
        {
            log.WriteLine("Количество студентов в группе " + group_name + " больше 5 и равно " + actual_count);
        }
    }

    class Printer
    {
        public Printer()
        { 
            EventManager.InDepartamentStudentsLessThan7 += new InDepartamentStudentsLessThan7(Print_InDepartamentStudentsLessThan7);
            EventManager.InGroupStudentsMoreThan5 += new InGroupStudentsMoreThan5(Print_InGroupStudentsMoreThan5);
        }

        public void Print_InDepartamentStudentsLessThan7(int actual_count)
        {
            Console.WriteLine("Количество студентов на кафедре меньше 7 и равно " + actual_count);
        }
        public void Print_InGroupStudentsMoreThan5(string group_name, int actual_count)
        {
            Console.WriteLine("Количество студентов в группе " + group_name + " больше 5 и равно " + actual_count);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = new Logger("log.txt");
            Printer printer = new Printer();
            
            Department department = new Department();

            StudentsGroup a = new StudentsGroup("a");
            StudentsGroup b = new StudentsGroup("b");
            StudentsGroup c = new StudentsGroup("c");

            department.StudentsGroups.Add(a);
            department.StudentsGroups.Add(b);
            department.StudentsGroups.Add(c);

            a.Add("Петров");
            a.Add("Простаков");
            a.Add("Зюлёв");

            b.Add("Подмазко");
            b.Add("Воейков");
            b.Add("Белоконь");
            b.Add("Мальчиков");
            b.Add("Климов");

            c.Add("Эпингер");
            c.Add("Онегин");
            c.Add("Клинских");
            c.Add("Туровский");


            a.Del("Петров");
            a.Del("Простаков");
            b.Del("Воейков");
            b.Del("Белоконь");
            c.Del("Клинских");
            c.Del("Туровский");
                  
            a.Add("Яресько");
            a.Add("Янкилович");
            a.Add("Поздов");
            a.Add("Меледин");
            b.Add("Малютин");
            a.Add("Филимонов");
            b.Add("Мандрыка");
            b.Add("Позон");
            c.Add("Ростовцев");
            a.Add("Паршиков");
        }
    }
}
